module bitbucket.org/mosteknoloji/robomotion-go-lib

go 1.14

require (
	github.com/golang/protobuf v1.4.2
	github.com/hashicorp/go-hclog v0.13.0
	github.com/hashicorp/go-plugin v1.2.2
	github.com/magiconair/properties v1.8.1
	github.com/tidwall/gjson v1.6.0
	github.com/tidwall/sjson v1.1.1
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120
	google.golang.org/grpc v1.29.1
)
